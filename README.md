# Awesome GNU/Linux [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)

> A curated list of awesome [blogs](#blogs) and [resources](#resources) in Persian for GNU/Linux OS.

## Contents
- [Blogs](#blogs)
- [Resources](#resources)
    - [Forums](#forums)
    - [Wikis](#wikis)
- [Community](#community)
    - [Telegram Groups](#telegram-groups)
    - [IRC channels](#irc-channels)
- [Projects](#projects)

## Blogs
- [jadi.net](https://jadi.net) - Jadi is one the key persons in spread the word about software freedom in Iran. Unfortunately, his blog is censored by Iranian government.
- [Soshaw.net](https://soshaw.net) - Sosha is a lover of GNU/Linux, his blog contains good tutorials about Tor, GPG, etc.
- [gnu.rocks](http://gnu.rocks) - gnu.rocks is Danial Behzadi's blog, who is one of the key translators in various Ubuntu projects.
- [Ali Molaei](https://molaei.org/) - Ali Molaei talks about software freedom.
- [Alireza Amirsamimi](http://amirsamimi.ir) - Alireza is a software developer and freedom lover. He created various video courses and [Persepolis](https://persepolisdm.github.io).

## Resources
### Forums
- [Ubuntu Iranian forum](http://forum.ubuntu.ir) - Official Ubuntu forum in Persian language. It's been active since 7 years ago.
- [ArchUsers](http://bbs.archusers.ir) - Arch Linux talkings are done in this forum.

### Wikis
- [Ubuntu Iranian wiki](http://wiki.ubuntu.ir) - Iran Ubuntu Wiki is a rich, resourceful wiki about Ubuntu and related softwares.
- [ArchUsers wiki](http://wiki.archusers.ir) - ArchUsers wiki is being translated by volunteers.

## Community
### Telegram Groups
- [GNU/Linux Q/A](https://t.me/joinchat/A-BT0j-v5COYi8sulrC0Bg)
- [Persian Ubuntu users chat](http://ubuntu.ir/telegram)

### IRC channels
- [Ubuntu Iran IRC](http://ubuntu.ir/irc.php)

## Projects
 - [Persepolis DM](https://persepolisdm.github.io/)
 - [Traktor](https://gitlab.com/TraktorPlus/Traktor)
 - [StarCalendar](https://github.com/ilius/starcal)



## License

[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)